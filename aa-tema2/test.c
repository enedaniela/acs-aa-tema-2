#include <stdio.h>
#include <stdlib.h>

/*functie ce interschimba doua elemente*/
void swap(int *a, int *b)
{
   int temp;
 
   temp = *b;
   *b   = *a;
   *a   = temp;   
}

void dutch_flag_sort(int *A, int n){
	int i = 0, j = 0;
	int N = n - 1;

	while(j <= N){
		/*se muta elementele mai mici decat 1 la stanga*/
		if(A[j] < 1){
			swap(&A[i],&A[j]);
			i++;
			j++;
		}
		else{
		/*se muta elementele mai mari decat 1 la dreapta*/	
			if(A[j] > 1){
				swap(&A[j],&A[N]);
				N--;
			}
			else
				j++;
		}
	}
	
}

int main(int argc, char **argv)
{
	FILE *inFile, *outFile;
	int *A, i, n;
	
/*se deschid fisierele*/
	inFile = fopen(argv[1],"r");
	outFile = fopen("cat-sort-b-result", "w");

	if ( !(inFile = fopen(argv[1],"r")) || !(outFile = fopen("cat-sort-b-result", "w")) ) {
		puts("Fisierele nu pot fi deschise");
	return 1;
	} /*eroare daca fisierele nu pot fi deschise*/
/*se aloca spatiu pentru vector si se citesc elementele*/
	fscanf(inFile, "%d", &n);
	A = malloc(n*sizeof(int));
	for (i = 0; i < n; i++)
    {
        fscanf(inFile, "%d1", &A[i]);
    }
	dutch_flag_sort(A,n);
/*se scrie vectorul sortat in fisierul de ouput*/	
	for(i = 0; i < n; i++){
        fprintf(outFile, "%d ", A[i]);
	}
	fprintf(outFile,"\n");
/*se inchid fisierele*/
	fclose(inFile);
	fclose(outFile);

	return 0;
}