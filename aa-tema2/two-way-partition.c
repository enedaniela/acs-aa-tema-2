#include <stdio.h>
#include <stdlib.h>

void counting_sort(int *A, int n){
	int i = 0;
	int white = 1, black = 1, striped = 1;
/*prima parcurgere numara cate valori de 0, 1 si respectiv 2*/
	for(i = 0; i < n; i++){
		if(A[i]==0){
			white++;
		}
		if(A[i]==1){
			black++;
		}
		if(A[i]==2){
			striped++;
		}
	}
/*a doua parcurgere suprascrie vectorul initial*/	
	for(i = 0; i < n; i++){
		if(white){
			A[i] = 0;
			white--;
		}
		if(black && !white){
			A[i] = 1;
			black--;
		}
		if(striped && !white && !black){
			A[i] = 2;
			striped--;
		}		
	}	
}

int main(int argc, char **argv)
{
	FILE *inFile, *outFile;
	int *A, i, n;

/*se deschid fisierele*/
	inFile = fopen(argv[1],"r");
	outFile = fopen("cat-sort-a-result", "w");

	if ( !(inFile = fopen(argv[1],"r")) || !(outFile = fopen("cat-sort-a-result", "w")) ) {
		puts("Fisierele nu pot fi deschise");
	return 1;
	} /*eroare daca fisierele nu pot fi deschise*/
/*se aloca spatiu pentru vector si se citesc elementele*/ 
	fscanf(inFile, "%d", &n);
	A = malloc(n*sizeof(int));
	for (i = 0; i < n; i++)
    {
        fscanf(inFile, "%d1", &A[i]);
    }
	counting_sort(A,n);
/*se scrie vectorul sortat in fisierul de ouput*/
	for(i = 0; i < n; i++){
        fprintf(outFile, "%d ", A[i]);
	}
	fprintf(outFile,"\n");
/*se inchid fisierele*/
	fclose(inFile);
	fclose(outFile);

	return 0;
}